import cv2
import numpy as np
import math


def horizont_angle(file):
    angles = []

    img = cv2.cvtColor(file, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(img, (5, 5), 0)
    edges = cv2.Canny(blur, 15, 100, L2gradient=False)
    lines = cv2.HoughLinesP(edges, 1, math.pi / 180.0,
                            100, minLineLength=100, maxLineGap=5)

    for [[x1, y1, x2, y2]] in lines:
        cv2.line(img, (x1, y1), (x2, y2), (255, 0, 0), 3)
        angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
        angles.append(angle)
    median_angle = np.median(angles)

    return median_angle


def img_rotate(img, angle):
    (h, w) = img.shape[:2]
    center = (int(w/2), int(h/2))
    rotation_matrix = cv2.getRotationMatrix2D(center, angle, 1)
    img_r = cv2.warpAffine(img, rotation_matrix, (w, h))

    # deg to radians
    rad = abs(angle) * math.pi / 180
    tan = math.tan(rad)
    cut_h = int(h * tan)
    cut_w = int(w * tan)
    # Обрезка черных углов
    img_rotated = img_r[h-(h-cut_h):h-cut_h, w-(w-cut_w):w-cut_w]

    return img_rotated


# Resize the images:
def resizing(img, new_width=None, new_height=None, interp=cv2.INTER_LINEAR):
    h, w = img.shape[:2]

    if new_width is None and new_height is None:
        return img

    if new_width is None:
        ratio = new_height / h
        dimension = (int(w * ratio), new_height)

    else:
        ratio = new_width / w
        dimension = (new_width, int(h * ratio))

    res_img = cv2.resize(img, dimension, interpolation=interp)
    return res_img


def avg(data, step, type="lst"):
    if type == "dic":
        data_tmp = []
        for k, v in data.items():
            data_tmp.append(int(v))
        data = data_tmp
        type = "lst"
    if type == "lst":
        for i in range((len(data) + step - 1) // step):
            sublist = data[i*step:(i+1)*step]
            yield int(sum(sublist) / len(sublist))
