import sys
import cv2
import os
import time
import numpy as np
from helpers import horizont_angle

def video2frames(file_name, folder, step=5):
    # Read the video from specified path:
    file = folder +"\\"+ file_name
    print(file)
    cam = cv2.VideoCapture(file)

#------------------------------------------------------------------------------
    # try:
    #     # creating a folder named data
    #     if not os.path.exists('data'):
    #         os.makedirs('data')
    #
    # # if not created then raise error
    # except OSError:
    #     print ('Error: Creating directory of data')

#------------------------------------------------------------------------------
    # frame
    currentframe = 0
    frames = range(0, 10000, int(step))
    print(frames)
    angle = []
    exp_frames = {}

    while(True):

        # reading from frame
        ret,frame = cam.read()

        if ret:
            # if video is still left continue creating images
            name = 'fr' + str(currentframe).zfill(5)
            print ('Reading...' + name)

            # writing the extracted images
            if currentframe in frames:
                exp_frames[name] = frame
                print ('\nWritten...' + name)

            currentframe += 1
        else:
            break

    cam.release()
    return exp_frames
