import cv2
import numpy as np


def square_picture(name, src):
    # src = cv2.imread(img)
    h, w = src.shape[:2]

    # set a new height in pixels
    new_width = src.shape[0]

    new_hight = src.shape[1]

    if h > w:
        # dsize
        dsize = (src.shape[0], new_width)
    else:
        dsize = (new_hight, w)
    # resize image
    output = cv2.resize(src, dsize, interpolation=cv2.INTER_AREA)

    # cv2.imshow("Image_res", resizing(output, 600))
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    return output


def mydefisheyemtx(img, img_orig):
    h, w = img.shape[:2]
    src = img

    h_orig, w_orig = img_orig.shape[:2]
    # получаем высоту и ширину изображения для
    # print(h, w)

    # заполняем матрицу преобразования. сначала все нулями
    intrinsics = np.zeros((3, 3), np.float64)

    # матрица intrinsics
    intrinsics[0, 0] = 5000
    intrinsics[1, 1] = 5000
    intrinsics[2, 2] = 1
    intrinsics[0, 2] = w/2
    intrinsics[1, 2] = h/2

    newCamMtx = np.zeros((3, 3), np.float64)
    newCamMtx[0, 0] = 5000
    newCamMtx[1, 1] = 5000
    newCamMtx[2, 2] = 1
    newCamMtx[0, 2] = h/2  # Расположение x
    newCamMtx[1, 2] = w/2  # Расположение y

    dist_coeffs = np.zeros((1, 4), np.float64)

    if w_orig < h_orig:
        dist_coeffs[0, 0] = -0.3  # -0.3
        dist_coeffs[0, 1] = -2  # -2
        dist_coeffs[0, 2] = 0
        dist_coeffs[0, 3] = 0
    else:
        dist_coeffs[0, 0] = -0.8  # -0.3
        dist_coeffs[0, 1] = -2  # -2
        dist_coeffs[0, 2] = 0
        dist_coeffs[0, 3] = 0

    map1, map2 = cv2.initUndistortRectifyMap(
        intrinsics, dist_coeffs, None, newCamMtx, src.shape[:2], cv2.CV_16SC2
        )
    res = cv2.remap(src, map1, map2, cv2.INTER_LINEAR)

    # resize image
    res = cv2.resize(res, (w_orig, h_orig), interpolation=cv2.INTER_AREA)

    cv2.imshow("frame", res)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    # cv2.imshow("Image_res", resizing(res, 800))
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    return res
