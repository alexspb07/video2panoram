import cv2
import os
import math
import time
import numpy as np
import gc
from queue import Queue
from threading import Thread
from helpers import horizont_angle, img_rotate, resizing, avg
from coord import keypoints_creator
from video2frames import video2frames
from mydefisheyemtx import square_picture, mydefisheyemtx
from mymoviepy import my_movie_py


def frame_normalization(frames, folder, out_queue, show_process=0):
    # Normalization of the frames
    angles = []
    temp = []
    for key, value in frames.items():
        # Defisheye of the pictures
        # Make square of the frame (for defisheye)
        sqr_pic = square_picture(key, value)
        # Defisheye
        mdf = mydefisheyemtx(sqr_pic, value)
        frames[key] = mdf
        # Finding the frame angles of rotation
        ang = horizont_angle(mdf)
        angles.append(ang)  # List of the rotation angles
        print(f'Normalization of the frame {key} is done')

    # Calculation of the frame angles of rotation
    print('Rotation of the frames...')
    for i in angles:
        if 0 < abs(i) < 5:  # Searching values only for horizontal and vertical lines
            temp.append(i)
    ang = int(np.mean(temp))  # Average anlge of rotation
    print(f'Angle = {ang}deg')

    # Rotation of the frames
    for key, value in frames.items():
        img_rtt = img_rotate(value, ang)
        frames[key] = img_rtt
        print(f'Frame {key} has been rotated')
        if show_process == 1:
            cv2.imwrite(f'{folder}\data\{key}.jpg', img_rtt)

    return frames  # Dictionary with normalized frames


def pano_analisys(frames, folder, out_queue, show_process=0):
    # Panoram analisys
    counter = 0
    step_dict = {}

    for key, img in frames.items():  # key - name of the frame, img - frame (npy)
        print("\nFrame № ", counter, " from ", len(frames))

        # First iteration (Searching the primary frame):
        if counter == 0:
            pano_new = img
            img_file_previous = key
            counter += 1

        else:
            # Searching the keypoints
            step1 = keypoints_creator(
                frames[img_file_previous], img, key, show_process, "ORB")
            step_dict[key] = step1  # Step of the frames displacements
            print("ORB = ", step1)
            counter += 1
            img_file_previous = key  # Return the frame for next itteration

    if show_process == 2:
        with open("step_dictionary.txt", 'a', encoding='utf-8') as f:
            for k, v in step_dict.items():
                f.write(f'{k} {v}\n')  # Name of the frame + displacement

    # Searching the average displacement of the frames
    avg_spd_lst = list(avg(step_dict, math.ceil((25/step_fps*2)), "dic"))
    avg_spd_lst_dic = {}

    for count, value in enumerate(avg_spd_lst):
        avg_spd_lst_dic[count] = value

    avg_spd = int(np.mean(avg_spd_lst))  # Avg speed for the full video

    # Admittion of the status frame:
    counter_tmp = 0
    for k, v in step_dict.items():
        v1 = avg_spd_lst_dic[counter_tmp//math.ceil((25/step_fps*2))]
        if abs(v1*0.6) < abs(v) < abs(v1*1.5):
            step_dict[k] = [v, "action", v1]
        else:
            step_dict[k] = [v, "stb", v1]
        counter_tmp += 1

    # Definding the orientation of the panoram
    if avg_spd < 0:
        pano_side = "left"  # From right to left side |Generation <----
    else:
        pano_side = "right"  # From left to right side |Generation ---->

    out_queue.put([step_dict, pano_side])


def left_pano_side(img1, img2, counter, show_process=0):
    # LEFT PANO
    img1_h, img1_w = img1.shape[:2]
    img2_h, img2_w = img2.shape[:2]

    if counter <= 1:
        img1_c = img1[:, int(img1_w*0.5):]  # First frame - only half
    else:
        img1_c = img1  # Next frames - full size

    # Joined frame - 80% of the size
    img2_c = img2[:, int(img2_w*0.1):int(img2_w*0.9)]

    img1_h, img1_w = img1_c.shape[:2]
    img2_h, img2_w = img2_c.shape[:2]

    # print(f'Cut Pic1: {img1_h}, {img1_w}')
    # print(f'Cut Pic2: {img2_h}, {img2_w}')

    if show_process == 1:
        cv2.imshow("img1 - L",
                   resizing(img1_c, None, 800))
        cv2.imshow("img2 - L", resizing(img2_c, None, 800))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    if counter <= 1:
        # If pano doesn't exist - gen. new template with 50% of the second frame
        # and step of the frame
        img3 = np.zeros(
            (img1_h, img1_w+abs(step)+int(img2_w*0.5), 3), np.uint8)
        msg = (
            f'Pano: {img1_h}, {img1_w+abs(step)+math.ceil(img2_w*0.5)}')
    else:
        # Pano exists - generate new template with step of the frame
        img3 = np.zeros(
            (img1_h, img1_w+abs(step), 3), np.uint8)
        msg = (f'Pano: {img1_h}, {img1_w+abs(step)}')

    img3_h, img3_w = img3.shape[:2]
    img3[0:img1_h, img3_w-img1_w:, ] = img1_c  # First frame on the template
    img3[:, 0:math.ceil(img2_w)] = img2_c  # Second frame on the template
    print(msg)

    return img3


def right_pano_side(img1, img2, counter, show_process=0):
    # RIGHT PANO
    img1_h, img1_w = img1.shape[:2]
    img2_h, img2_w = img2.shape[:2]

    if counter <= 1:
        img1_c = img1[:, 0:int(img1_w*0.5)]
    else:
        img1_c = img1

    img2_c = img2[:, int(img2_w*0.1):int(img2_w*0.9)]

    img1_h, img1_w = img1_c.shape[:2]
    img2_h, img2_w = img2_c.shape[:2]

    if show_process == 1:
        cv2.imshow("img1 - R",
                   resizing(img1_c, None, 800))
        cv2.imshow("img2 - R", resizing(img2_c, None, 800))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    if counter <= 1:
        img3 = np.zeros(
            (img1_h, img1_w+abs(step)+int(img2_w*0.5), 3), np.uint8)
        img3[0:img1_h, 0:img1_w, ] = img1_c
        img3[:, int(img1_w)-math.ceil(img2_w*0.5)
             + abs(step):] = img2_c
        print(
            f'Pano: {img1_h}, {img1_w+abs(step)+math.ceil(img2_w*0.5)}')

    else:
        img3 = np.zeros(
            (img1_h, img1_w+abs(step), 3), np.uint8)
        img3[0:img1_h, 0:img1_w, ] = img1_c
        img3[:, int(img1_w)+abs(step)
             - math.ceil(img2_w):] = img2_c
        print(f'Pano: {img1_h}, {img1_w+abs(step)}')

        if show_process == 1:
            cv2.imshow("Pano", resizing(img3, None, 500))
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    return img3

# ------------------------Open files-----------------------------------------


show_process = 0
step_fps = 1

# folder = input('Folder: ')
# file_input = input("File name: ")
# step_fps = input('Number of frames: ')
folder = "D:\\#Python\\TransOil\\Panoram\\Pano_video"

for subdir, dirs, files in os.walk(folder):
    for item in os.listdir(folder):
        time_start = time.time()
        cap = cv2.VideoCapture(item)
        fps = cap.get(cv2.CAP_PROP_FPS)
        print(cap, fps)

        # Creation of the frames from video
        # frames = video2frames(item, folder, step_fps)
        frames = my_movie_py(item, folder, step_fps)

        print("Video has been read")

        # Normalization of the frame
        frames = frame_normalization(frames, folder, None)

        frames1 = dict(list(frames.items())[:(len(frames)+1)//3])
        frames2 = dict(list(frames.items())[
                       ((len(frames)-2)//3):((len(frames)+2)//3)*2])
        frames3 = dict(list(frames.items())[((len(frames)-1)//3)*2:])

        print(frames1.keys())
        print(frames2.keys())
        print(frames3.keys())

        out_queue1 = Queue()
        out_queue2 = Queue()
        out_queue3 = Queue()

        thread1 = Thread(target=pano_analisys, args=(
                        frames1, folder, out_queue1, None))
        thread2 = Thread(target=pano_analisys, args=(
                        frames2, folder, out_queue2, None))
        thread3 = Thread(target=pano_analisys, args=(
                        frames3, folder, out_queue3, None))

        thread1.start()
        thread2.start()
        thread3.start()

        thread1.join()
        thread2.join()
        thread3.join()

        frames1 = out_queue1.get()
        frames2 = out_queue2.get()
        frames3 = out_queue3.get()

        print(frames1)
        print(frames2)
        print(frames3)

        pano_side = frames1[1]
        frames1[0].update(frames2[0])
        frames1[0].update(frames3[0])
        step_dict = frames1[0]

        print(step_dict)

        """------------ PANO creating ------------"""
        counter = 0
        step_previous = None
        pano_new_name = None
        action = None

        for key, img in frames.items():
            # If panorama doesn't exist, take a first capture:
            print("Frame №", counter, " from ", len(frames))
            if not pano_new_name:
                pano_new_name = (
                    f'{folder}\\pano_{item[:-4]}-{step_fps}fps.jpg')
                pano_new = img
                counter += 1
                img_file_previous = key
                print(f'\nPano: {pano_new_name}')

            # Iterate files form folder:
            else:
                # print(f'\nFirst img: {img_file_previous}')
                # print(f'Second img: {key}')

                step_data = step_dict[key]
                # step = avg_spd_lst_dic[counter//math.ceil((25/step_fps)*2)]
                step = int(step_data[0])
                action = step_data[1]

                if action == "stb":
                    step = int(step_data[2])
                    # print("updated step = ", step)

                # print(f'Step of frame = {step}')

                if show_process == 1:
                    cv2.imshow(img_file_previous, resizing(
                        frames[img_file_previous], None, 800))
                    cv2.imshow(key, resizing(frames[key], None, 800))
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                img1 = pano_new
                img2 = frames[key]

                if pano_side == "left":
                    img3 = left_pano_side(img1, img2, counter, show_process)
                else:
                    img3 = right_pano_side(img1, img2, counter, show_process)

                pano_new = img3
                counter += 1
                # print(f'Frame number: {counter} added sucessfully!')
                img_file_previous = key

        cv2.imwrite(pano_new_name, img3)
        if show_process == 2:
            with open(f'{pano_new_name}.txt', 'a', encoding='utf-8') as f:
                f.write(f'\n{item}\n')
                for k, v in step_dict.items():
                    f.write(f'{k} {v}\n')

        time_finish = time.time()
        gc.collect()
        print(
            f'Panoram of the {item} is done in {(time_finish-time_start)/60} min.')
