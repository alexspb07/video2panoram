import sys
import threading
import cv2
import os
import time
import numpy as np
from helpers import horizont_angle, img_rotate, resizing


# Seraching the matching points:
def keypoints_creator(img1_input, img2_input, img2_name, show_process, detector="ORB"):

    img1 = cv2.cvtColor(img1_input, cv2.COLOR_BGR2GRAY)
    img2 = cv2.cvtColor(img2_input, cv2.COLOR_BGR2GRAY)

    # Pic size
    img1_h, img1_w = img1.shape[:2]
    img2_h, img2_w = img2.shape[:2]

    # Create the the object distinctive points
    if detector == "ORB":
        # ORB
        orb = cv2.ORB_create(nfeatures=2000)
    else:
        # SIFT
        sift = cv2.SIFT_create(2000)

    # Создание маски
    img1_mask = img1.copy()
    img2_mask = img2.copy()

    img1_mask[:, :] = (0)
    img2_mask[:, :] = (0)

    img1_mask[int(img1_h*0.25):int(img1_h*0.8):, :] = (255)
    img2_mask[int(img2_h*0.25):int(img2_h*0.8), :] = (255)

    if show_process == 1:
        cv2.imshow("img1_mask", resizing(img1_mask, None, 500))
        cv2.imshow("img2_mask", resizing(img2_mask, None, 500))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    if detector == "ORB":
        # Generate and draw the references points ORB
        keypoints1, descriptors1 = orb.detectAndCompute(img1, img1_mask)
        keypoints2, descriptors2 = orb.detectAndCompute(img2, img2_mask)
    else:
        # Generate and draw the references points SIFT
        keypoints1, descriptors1 = sift.detectAndCompute(img1, img1_mask)
        keypoints2, descriptors2 = sift.detectAndCompute(img2, img2_mask)

    # It will find all of the matching keypoints on two images
    if detector == "ORB":
        # ORB
        bf = cv2.BFMatcher_create(cv2.NORM_HAMMING)
        # Find matching points
        matches = bf.knnMatch(descriptors1, descriptors2, k=2)
    else:
        # SIFT
        bf = cv2.BFMatcher()
        # Find matching points
    matches = bf.knnMatch(descriptors1, descriptors2, k=2)

    all_matches = []
    for m, n in matches:
        all_matches.append(m)
    print(f'Matches:{len(all_matches)}')

    if show_process == 1:

        kp_img1 = cv2.drawKeypoints(img1_mask, keypoints1, (255, 0, 255))
        kp_img2 = cv2.drawKeypoints(img2_mask, keypoints2, (255, 0, 255))

        # Show the points
        cv2.imshow("kp_img1", resizing(kp_img1, None, 500))
        cv2.waitKey(0)
        cv2.imshow("kp_img2", resizing(kp_img2, None, 500))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    displacement = []
    temp1 = []
    for match in all_matches:
        img1_idx = match.queryIdx
        img2_idx = match.trainIdx
        (x1, y1) = keypoints1[img1_idx].pt
        (x2, y2) = keypoints2[img2_idx].pt
        if -5 < (y2 - y1) < 5 and abs(x1 - x2) > 0:
            temp1.append([int(x1 - x2), int(y1 - y2)])
            displacement.append(int(x1 - x2))
    try:
        aver_temp1 = int(np.mean(displacement))

        temp2 = []
        temp_min = []
        temp_max = []
        for i in temp1:
            if 0 <= abs(int(i[0])) < aver_temp1*1.5:
                temp2.append(f'{i[0]}')

        for i in temp2:
            if abs(int(i)) < aver_temp1*0.3 or abs(int(i)) < 15:
                temp_min.append(i)

            elif aver_temp1*0.7 < abs(int(i)) < aver_temp1*1.3:
                temp_max.append(i)

        l_temp_min = len(temp_min)
        l_temp_max = len(temp_max)

        # with open(f"test.txt",'a',encoding = 'utf-8') as f:
        #     # f.write(f'\n{img2_name}, {detector}\nAver = {aver_temp1}\nMax = {l_temp_max} \nMin = {l_temp_min}\n{displacement}')
        #     f.write(f'\n{img2_name}, {detector}\n{displacement}')

        if l_temp_min >= l_temp_max:
            res = int(np.mean(displacement))
        else:
            res = int(np.mean(displacement))

        return res
    except:
        return 0


def draw_matches(img1, keypoints1, img2, keypoints2, matches):
    print("draw_matches")
    r, c = img1.shape[:2]
    r1, c1 = img2.shape[:2]

    # Create a blank image with the size of the first image + second image
    output_img = np.zeros((max([r, r1]), c+c1, 3), dtype='uint8')
    output_img[:r, :c, :] = np.dstack([img1, img1, img1])
    output_img[:r1, c:c+c1, :] = np.dstack([img2, img2, img2])

    # Go over all of the matching points and extract them
    match_coord = []
    for match in matches:
        img1_idx = match.queryIdx
        img2_idx = match.trainIdx
        (x1, y1) = keypoints1[img1_idx].pt
        (x2, y2) = keypoints2[img2_idx].pt

        # Draw circles on the keypoints
        coord1 = int(x1), int(y1)
        coord2 = int(x2)+c, int(y2)
        # print(coord1, (int(x2),int(y2)))

        cv2.circle(output_img, (int(x1), int(y1)), 4, (0, 255, 255), 1)
        cv2.putText(output_img, str(coord1), coord1,
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2, cv2.LINE_AA)
        cv2.circle(output_img, (int(x2)+c, int(y2)), 4, (0, 255, 255), 1)
        cv2.putText(output_img, str((int(x2), int(y2))), coord2,
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2, cv2.LINE_AA)
        match_coord.append(f'{int(x1)},{int(y1)}; {int(x2)},{int(y2)}')

        # Connect the same keypoints
        cv2.line(output_img, (int(x1), int(y1)),
                 (int(x2)+c, int(y2)), (0, 255, 0), 1)

    # with open(f"test-{counter}.txt",'w',encoding = 'utf-8') as f:
    #     for i in match_coord:
    #         f.write(f'{i}\n')

    return output_img
