
import cv2
from moviepy.editor import VideoFileClip


def my_movie_py(item, folder, step_fps):
    file = f"{folder}\\{item}"
    clip = VideoFileClip(file)
    count = 1
    exp_frames = {}
    for frame in clip.iter_frames():
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        exp_frames[count] = frame
        count += 1
        cv2.imshow("frame", frame)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    print(exp_frames)

    return exp_frames

# from mymoviepy import my_movie_py
# 221
# frames = my_movie_py(item, folder, step_fps)
